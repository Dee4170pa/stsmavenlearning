package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//Spring mvc class i.e grt,post,put,delete

@RestController
public class HelloWorldController {

	@GetMapping("/helloworld1")
	public String m1()
	{
		return "hi test 1 method";
		
	}
	@GetMapping("/helloword2")
	public Student getStudentDetails()
	{
		return new Student("deepa","reddy");
	}
}
